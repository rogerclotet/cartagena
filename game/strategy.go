package game

import "math/rand"

type strategy struct {
	name     string
	takeTurn turnFunc
}

type turnFunc func(playerIndex int, players []*player, d deck)

func randomStrategy() strategy {
	return strategy{
		name:     "random",
		takeTurn: randomTurnFunc,
	}
}

var randomTurnFunc turnFunc = func(playerIndex int, players []*player, d deck) {
	p := players[playerIndex]

	if len(p.cards) == 0 {
		// skip turn and draw a card
		// TODO implement going back to draw instead
		p.cards = []symbol{d.draw()}
	}

	occupiedPositions := make([]int, len(board))

	for _, cp := range players {
		for _, cpPos := range cp.pirates {
			if !cpPos.isFinished && !cpPos.isStartingPosition {
				occupiedPositions[cpPos.boardPosition]++
			}
		}
	}

	for action := 1; action <= 3; action++ {
		if len(p.cards) == 0 {
			// skip turn and draw a card
			// TODO implement going back to draw instead
			p.cards = []symbol{d.draw()}
		}

		pirateToMove := rand.Intn(len(p.pirates))
		pos := p.pirates[pirateToMove]

		usedCardIndex := rand.Intn(len(p.cards))
		usedCard := p.cards[usedCardIndex]
		p.cards = append(p.cards[:usedCardIndex], p.cards[usedCardIndex+1:]...)

		destination := useCard(pos.boardPosition, usedCard, occupiedPositions)

		if !pos.isStartingPosition && !pos.isFinished {
			occupiedPositions[pos.boardPosition]--
		}
		if destination < len(board) {
			occupiedPositions[destination]++
		}

		pos.boardPosition = destination
		pos.isFinished = destination >= len(board)
		pos.isStartingPosition = destination == 0
		p.pirates[pirateToMove] = pos
	}
}

func useCard(from int, card symbol, occupiedPositions []int) int {
	for pos := from + 1; pos < len(board); pos++ {
		if board[pos] == card && occupiedPositions[pos] < maxPiratesInPosition {
			return pos
		}
	}

	return len(board)
}
