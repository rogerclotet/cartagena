package game

type position struct {
	isStartingPosition bool
	isFinished         bool
	boardPosition      int
}
