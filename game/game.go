package game

// Game represents a Cartagena game
type Game interface {
	Simulate() Result
}

var board = []symbol{Hook, Parrot, Lamp, Gun, Chest, Rum, Parrot, Gun, Chest, Rum, Hook, Lamp, Chest, Lamp, Hook, Gun, Rum, Parrot, Lamp, Gun, Rum, Chest, Parrot, Hook, Gun, Parrot, Lamp, Hook, Chest, Rum, Chest, Gun, Hook, Lamp, Parrot, Rum}
var symbols = []symbol{Hook, Parrot, Lamp, Gun, Chest, Rum}

const startingPosition = -1
const cardsPerSymbol = 17
const maxPiratesInPosition = 3

// New creates a Game
func New(players int) Game {
	d := deck{}
	ps := make([]*player, players)
	for k := 0; k < players; k++ {
		startingPirates := []position{
			position{boardPosition: -1, isStartingPosition: true},
			position{boardPosition: -1, isStartingPosition: true},
			position{boardPosition: -1, isStartingPosition: true},
			position{boardPosition: -1, isStartingPosition: true},
			position{boardPosition: -1, isStartingPosition: true},
			position{boardPosition: -1, isStartingPosition: true},
		}
		startingCards := []symbol{
			d.draw(),
			d.draw(),
			d.draw(),
			d.draw(),
			d.draw(),
			d.draw(),
		}
		ps[k] = &player{
			strategy: randomStrategy(),
			pirates:  startingPirates,
			cards:    startingCards,
		}
	}

	return &game{
		players: ps,
		deck:    d,
	}
}

type game struct {
	players []*player
	deck    deck
}

func (g *game) Simulate() Result {
	var turn int
	var finished bool
	var winningStrategy string
	for !finished {
		currentPlayerPosition := turn % len(g.players)
		currentPlayer := g.players[currentPlayerPosition]

		currentPlayer.strategy.takeTurn(currentPlayerPosition, g.players, g.deck)

		finished = currentPlayer.finished()
		if finished {
			winningStrategy = currentPlayer.strategy.name
		}

		turn++
	}

	return Result{
		Turns:           turn,
		WinningStrategy: winningStrategy,
	}
}
