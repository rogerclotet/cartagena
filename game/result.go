package game

// Result represents the outcome of a game
type Result struct {
	Turns           int
	WinningStrategy string
}
