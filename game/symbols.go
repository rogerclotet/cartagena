package game

type symbol int

const (
	// Hook is a game symbol
	Hook symbol = iota
	// Parrot is a game symbol
	Parrot
	// Lamp is a game symbol
	Lamp
	// Gun is a game symbol
	Gun
	// Chest is a game symbol
	Chest
	// Rum is a game symbol
	Rum
)
