package game

import "math/rand"

type deck struct {
	cards []symbol
}

func (d *deck) draw() symbol {
	if len(d.cards) == 0 {
		d.cards = generateDeck()
	}

	card := d.cards[0]
	d.cards = d.cards[1:]

	return card
}

func generateDeck() []symbol {
	unshuffledDeck := make([]symbol, cardsPerSymbol*len(symbols))
	for _, s := range symbols {
		for i := 0; i < cardsPerSymbol; i++ {
			unshuffledDeck[i] = s
		}
	}

	initialDeck := make([]symbol, len(unshuffledDeck))
	perm := rand.Perm(len(unshuffledDeck))
	for i, v := range perm {
		initialDeck[v] = unshuffledDeck[i]
	}

	return initialDeck
}
