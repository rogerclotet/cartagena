package game

type player struct {
	strategy strategy
	pirates  []position
	cards    []symbol
}

func (p player) finished() bool {
	for _, pos := range p.pirates {
		if !pos.isFinished {
			return false
		}
	}

	return true
}
