package main

import (
	"flag"
	"fmt"

	"gitlab.com/rogerclotet/cartagena/game"
)

func main() {
	var (
		players     int
		simulations int
		quiet       bool
	)

	flag.IntVar(&players, "players", 10, "Amount of players to simulate")
	flag.IntVar(&simulations, "simulations", 1000, "Number of simulations")
	flag.BoolVar(&quiet, "quiet", false, "Hide output")
	flag.Parse()

	fmt.Printf("Simulating %d games with %d players...\n\n", simulations, players)

	for i := 1; i <= simulations; i++ {
		g := game.New(players)
		res := g.Simulate()

		if !quiet {
			fmt.Printf("- %d/%d: %s (%d turns)\n", i, simulations, res.WinningStrategy, res.Turns)
		}
	}

	fmt.Printf("\n\nSimulation complete\n")

	// TODO Print result summary
}
