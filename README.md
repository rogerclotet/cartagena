Cartagena Simulator
===================

This is a simulator for the board game Cartagena, with a simple setup, aiming to find out which strategy works better when playing the game, and whether picking one strategy or another is significant.
